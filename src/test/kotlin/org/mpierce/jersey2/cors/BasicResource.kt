package org.mpierce.jersey2.cors

import javax.ws.rs.*

@Path("resource")
class BasicResource {
    @GET
    fun get(): String {
        return "get"
    }

    @POST
    fun post(): String {
        return "post"
    }

    @DELETE
    fun delete(): String {
        return "delete"
    }

    @OPTIONS
    fun options(): String {
        return "options"
    }
}
