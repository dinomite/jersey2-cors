package org.mpierce.jersey2.cors.feature

import org.eclipse.jetty.server.ServerConnector
import org.eclipse.jetty.servlet.ServletContextHandler
import org.eclipse.jetty.servlet.ServletHolder
import org.glassfish.jersey.servlet.ServletContainer
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Before
import org.junit.Test
import org.mpierce.jersey2.cors.BasicResource
import org.mpierce.jersey2.cors.CorsHeaders
import org.mpierce.jersey2.cors.FixedSimpleRequestPolicy
import org.mpierce.jersey2.cors.TestJerseyApp
import javax.ws.rs.core.Response.Status

class ApplySimpleCorsResponseHeadersGloballyFeatureTest : FeatureTest() {

    @Before
    fun setUp() {
        var servletHolder = ServletHolder(ServletContainer(
                TestJerseyApp(listOf(
                        ApplySimpleCorsResponseHeadersGloballyFeature(
                                FixedSimpleRequestPolicy("*", emptyList(), false))),
                        listOf(BasicResource::class.java))))
        var handler = ServletContextHandler()
        handler.addServlet(servletHolder, "/*")

        server.handler = handler
        val connector = ServerConnector(server)
        server.addConnector(connector)
        server.start()

        port = connector.localPort
    }

    @After
    fun tearDown() {
        server.stop()
        client.close()
    }

    @Test
    fun testAppliesToGet() {
        val response = get("/resource")
        assertEquals(Status.OK.statusCode, response.statusCode)
        assertEquals("*", response.getHeader(CorsHeaders.ALLOW_ORIGIN))
    }

    @Test
    fun testAppliesToPost() {
        val response = post("/resource")
        assertEquals(Status.OK.statusCode, response.statusCode)
        assertEquals("*", response.getHeader(CorsHeaders.ALLOW_ORIGIN))
    }

    @Test
    fun testDoesntApplyToOptions() {
        val response = options("/resource")
        assertEquals(Status.OK.statusCode, response.statusCode)
        assertFalse(response.headers.containsKey(CorsHeaders.ALLOW_ORIGIN))
    }

    @Test
    fun testDoesntApplyToDelete() {
        val response = delete("/resource")
        assertEquals(Status.OK.statusCode, response.statusCode)
        assertFalse(response.headers.containsKey(CorsHeaders.ALLOW_ORIGIN))
    }
}
