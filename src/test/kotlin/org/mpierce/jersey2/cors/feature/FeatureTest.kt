package org.mpierce.jersey2.cors.feature

import com.ning.http.client.AsyncHttpClient
import com.ning.http.client.Response
import org.eclipse.jetty.server.Server
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.mpierce.jersey2.cors.CorsHeaders
import javax.ws.rs.core.HttpHeaders

open class FeatureTest {
    val server = Server()
    val client = AsyncHttpClient()

    var port: Int = -1

    fun options(path: String): Response {
        val boundRequestBuilder = client.prepareOptions("http://localhost:" + port + path)
        boundRequestBuilder.addHeader(CorsHeaders.ORIGIN, "http://localhost")
        return boundRequestBuilder.execute().get()!!
    }

    fun post(path: String): Response {
        val boundRequestBuilder = client.preparePost("http://localhost:" + port + path)
        boundRequestBuilder.addHeader(CorsHeaders.ORIGIN, "http://localhost")
        boundRequestBuilder.addHeader(HttpHeaders.CONTENT_TYPE, "text/plain")
        return boundRequestBuilder.execute().get()!!
    }

    fun get(path: String): Response {
        val boundRequestBuilder = client.prepareGet("http://localhost:" + port + path)
        boundRequestBuilder.addHeader(CorsHeaders.ORIGIN, "http://localhost")
        return boundRequestBuilder.execute().get()!!
    }

    fun delete(path: String): Response {
        val boundRequestBuilder = client.prepareDelete("http://localhost:" + port + path)
        boundRequestBuilder.addHeader(CorsHeaders.ORIGIN, "http://localhost")
        return boundRequestBuilder.execute().get()!!
    }

    fun assertNoPreflightHeaders(response: Response) {
        assertFalse(response.headers.containsKey(CorsHeaders.ALLOW_ORIGIN))
        assertFalse(response.headers.containsKey(CorsHeaders.MAX_AGE))
        assertFalse(response.headers.containsKey(CorsHeaders.ALLOW_METHODS))
        assertFalse(response.headers.containsKey(CorsHeaders.ALLOW_HEADERS))
        assertFalse(response.headers.containsKey(CorsHeaders.ALLOW_CREDENTIALS))
    }

    fun assertDefaultPreflightHeaders(response: Response) {
        assertEquals("*", response.getHeader(CorsHeaders.ALLOW_ORIGIN))
        assertEquals("86400", response.getHeader(CorsHeaders.MAX_AGE))
    }
}