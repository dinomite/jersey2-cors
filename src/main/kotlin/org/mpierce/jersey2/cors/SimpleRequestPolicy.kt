package org.mpierce.jersey2.cors

import javax.ws.rs.container.ContainerRequestContext

/**
 * Controls how CORS headers are set in responses for simple (not preflight) requests.
 */
interface SimpleRequestPolicy {
    /**
     * Origins are of the form "*" or a url with no path (e.g. http://domain.com). Must not be "*" if allowCredentials returns true.
     *
     * @return value of the Access-Control-Allow-Origin header to be used in the response
     */
    fun getAllowOrigin(requestContext: ContainerRequestContext): String

    /**
     * If this returns true, the implementation of getAllowOrigin() must not return "*"
     *
     * @return true if the response should specify Access-Control-Allow-Credentials: true
     */
    fun allowCredentials(requestContext: ContainerRequestContext): Boolean

    /**
     * @return comma separated list of response headers to be exposed to the user agent, or empty string
     */
    fun exposeHeaders(requestContext: ContainerRequestContext): String

}
