package org.mpierce.jersey2.cors.feature

import org.mpierce.jersey2.cors.CorsPreflightResponseResourceFilter
import org.mpierce.jersey2.cors.PreflightRequestPolicy
import javax.ws.rs.OPTIONS
import javax.ws.rs.container.DynamicFeature
import javax.ws.rs.container.ResourceInfo
import javax.ws.rs.core.FeatureContext

/**
 * Applies a CorsSimpleResponseResourceFilter to all POST, GET, and HEAD resource methods with the provided policy.
 */
class ApplyPreflightCorsResponseHeadersGloballyFeature(policy: PreflightRequestPolicy) : DynamicFeature {
    private val filter = CorsPreflightResponseResourceFilter(policy)

    override fun configure(resourceInfo: ResourceInfo, context: FeatureContext) {
        val method = resourceInfo.getResourceMethod()
        if (method.getAnnotation(OPTIONS::class.java) != null) {
            context.register(filter)
        }
    }

}
