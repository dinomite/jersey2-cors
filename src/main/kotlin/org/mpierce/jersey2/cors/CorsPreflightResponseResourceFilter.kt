package org.mpierce.jersey2.cors

import javax.annotation.concurrent.ThreadSafe
import javax.ws.rs.container.ContainerRequestContext
import javax.ws.rs.container.ContainerResponseContext
import javax.ws.rs.container.ContainerResponseFilter

@ThreadSafe
class CorsPreflightResponseResourceFilter(private val policy: PreflightRequestPolicy) : ContainerResponseFilter {

    override fun filter(requestContext: ContainerRequestContext, responseContext: ContainerResponseContext) {
        val incomingOrigin = requestContext.getHeaderString(CorsHeaders.ORIGIN)
        if (incomingOrigin == null) {
            return
        }

        val h = responseContext.headers

        if (putIfNotPresent(h, CorsHeaders.ALLOW_ORIGIN, policy.getAllowOrigin(requestContext))
                && policy.shouldVaryOrigin(requestContext)) {
            // TODO add Origin to Vary
        }

        putIfNotPresent(h, CorsHeaders.MAX_AGE, policy.getMaxAge(requestContext))
        val allowMethods = policy.getAllowedHttpMethods(requestContext)
        if (!allowMethods.isEmpty()) {
            putIfNotPresent(h, CorsHeaders.ALLOW_METHODS, allowMethods)
        }
        val allowHeaders = policy.getAllowedHeaders(requestContext)
        if (!allowHeaders.isEmpty()) {
            putIfNotPresent(h, CorsHeaders.ALLOW_HEADERS, allowHeaders)
        }
        val allowCredentials = policy.credentialsAllowed(requestContext)
        if (allowCredentials) {
            putIfNotPresent(h, CorsHeaders.ALLOW_CREDENTIALS, allowCredentials.toString())
        }
    }
}
