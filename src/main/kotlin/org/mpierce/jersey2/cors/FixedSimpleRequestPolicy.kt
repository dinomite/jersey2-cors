package org.mpierce.jersey2.cors

import javax.ws.rs.container.ContainerRequestContext

class FixedSimpleRequestPolicy(private val allowOrigin: String = "*",
                               exposeHeaders: List<String> = emptyList(),
                               private val allowCredentials: Boolean = false) : SimpleRequestPolicy {

    private val exposeHeaders = exposeHeaders.joinToString(",")

    override fun getAllowOrigin(requestContext: ContainerRequestContext): String {
        return allowOrigin
    }

    override fun exposeHeaders(requestContext: ContainerRequestContext): String {
        return exposeHeaders
    }

    override fun allowCredentials(requestContext: ContainerRequestContext): Boolean {
        return allowCredentials
    }

}
