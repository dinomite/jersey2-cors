package org.mpierce.jersey2.cors

object CorsHeaders {
    val ALLOW_ORIGIN = "Access-Control-Allow-Origin"
    val EXPOSE_HEADERS = "Access-Control-Expose-Headers"
    val MAX_AGE = "Access-Control-Max-Age"
    val ALLOW_CREDENTIALS = "Access-Control-Allow-Credentials"
    val ALLOW_METHODS = "Access-Control-Allow-Methods"
    val ALLOW_HEADERS = "Access-Control-Allow-Headers"
    val ORIGIN = "Origin"

    // Request preflight headers:
    // TODO Access-Control-Request-Method
    // TODO Access-Control-Request-Headers
}
