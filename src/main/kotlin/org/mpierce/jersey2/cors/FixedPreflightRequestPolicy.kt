package org.mpierce.jersey2.cors

import javax.ws.rs.container.ContainerRequestContext

class FixedPreflightRequestPolicy (private val allowedOrigin: String = "*",
                            allowedHeaders: List<String> = emptyList(),
                            allowedHttpMethods: List<String> = listOf("GET"),
                            private val credentialsAllowed: Boolean = false,
                            maxAge: Int = 24 * 3600) : PreflightRequestPolicy {
    private val maxAge = Integer.toString(maxAge)
    private val allowMethods = allowedHttpMethods.joinToString(",")
    private val allowHeaders = allowedHeaders.joinToString(",")

    override fun getAllowedHeaders(requestContext: ContainerRequestContext): String {
        return allowHeaders
    }

    override fun getAllowedHttpMethods(requestContext: ContainerRequestContext): String {
        return allowMethods
    }

    override fun getAllowOrigin(requestContext: ContainerRequestContext): String {
        return allowedOrigin
    }

    override fun getMaxAge(requestContext: ContainerRequestContext): String {
        return maxAge
    }

    override fun shouldVaryOrigin(requestContext: ContainerRequestContext): Boolean {
        return false
    }

    override fun credentialsAllowed(requestContext: ContainerRequestContext): Boolean {
        return credentialsAllowed
    }

}
