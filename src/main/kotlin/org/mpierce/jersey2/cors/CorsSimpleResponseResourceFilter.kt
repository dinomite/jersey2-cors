package org.mpierce.jersey2.cors

import org.mpierce.jersey2.cors.CorsSimpleMediaTypes.SIMPLE_MEDIA_TYPES
import javax.annotation.concurrent.ThreadSafe
import javax.ws.rs.HttpMethod
import javax.ws.rs.container.ContainerRequestContext
import javax.ws.rs.container.ContainerResponseContext
import javax.ws.rs.container.ContainerResponseFilter
import javax.ws.rs.core.HttpHeaders

@ThreadSafe
class CorsSimpleResponseResourceFilter(private val policy: SimpleRequestPolicy) : ContainerResponseFilter {

    override fun filter(requestContext: ContainerRequestContext, responseContext: ContainerResponseContext) {
        val incomingOrigin = requestContext.getHeaderString(CorsHeaders.ORIGIN)
        if (incomingOrigin == null) {
            return
        }

        if (requestContext.method == HttpMethod.POST) {
            val contentType: String? = requestContext.getHeaderString(HttpHeaders.CONTENT_TYPE)
            if (contentType == null || !containsAny(contentType, SIMPLE_MEDIA_TYPES)) {
                return
            }
        }

        val allowOrigin = policy.getAllowOrigin(requestContext)
        val allowCredentials = policy.allowCredentials(requestContext)

        if (allowCredentials && allowOrigin.equals("*")) {
            // TODO tests
            throw IllegalStateException("Cannot have allowCredentials = true and allowOrigin = *")
        }

        val h = responseContext.headers

        putIfNotPresent(h, CorsHeaders.ALLOW_ORIGIN, allowOrigin)

        val exposeHeaders = policy.exposeHeaders(requestContext)
        if (!exposeHeaders.isEmpty()) {
            putIfNotPresent(h, CorsHeaders.EXPOSE_HEADERS, exposeHeaders)
        }

        if (allowCredentials) {
            putIfNotPresent(h, CorsHeaders.ALLOW_CREDENTIALS, "true")
        }
    }

    private fun containsAny(str: String, searchStrs: List<String>): Boolean {
        searchStrs.forEach {
            if (str.contains(it)) {
                return true
            }
        }

        return false
    }
}

