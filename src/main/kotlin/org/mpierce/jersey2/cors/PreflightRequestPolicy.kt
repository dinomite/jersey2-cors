package org.mpierce.jersey2.cors;

import javax.annotation.concurrent.ThreadSafe
import javax.ws.rs.container.ContainerRequestContext

/**
 * Controls how CORS headers are set in preflight responses.
 */
@ThreadSafe
interface PreflightRequestPolicy {

    /**
     * Origins are of the form "*" or a url with no path (e.g. http://domain.com).
     *
     * @return value of the Access-Control-Allow-Origin header
     */
    fun getAllowOrigin(requestContext: ContainerRequestContext): String

    /**
     * @return true if the value returned by getAllowOrigin() changes based on origin, and therefore Origin should be
     * added to the Vary header
     */
    fun shouldVaryOrigin(requestContext: ContainerRequestContext): Boolean

    /**
     * @return client cache expiration time for preflight CORS info
     */
    fun getMaxAge(requestContext: ContainerRequestContext): String

    /**
     * @return http methods allowed for the future requests to the url, comma separated
     */
    fun getAllowedHttpMethods(requestContext: ContainerRequestContext): String

    /**
     * @return http request header names allowed to be present in future requests to the url, comma separated
     */
    fun getAllowedHeaders(requestContext: ContainerRequestContext): String

    /**
     * @return true if credentials should be allowed in future requests to the url
     */
    fun credentialsAllowed(requestContext: ContainerRequestContext): Boolean
}
